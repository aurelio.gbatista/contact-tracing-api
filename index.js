require('dotenv').config({
  path: `.env.${process.env.NODE_ENV}`
});
// ---------------------------------------BANCO---------------------------------------

const mongoose = require('mongoose'),
  { Schema } = mongoose;

let options = {
  keepAlive: true,
  connectTimeoutMS: 300000,
  socketTimeoutMS: 300000,
  bufferCommands: true,
  maxPoolSize: 2,
  useNewUrlParser: true,
  useUnifiedTopology: true
};

const mongoUrl = `mongodb://${process.env.MONGO_USER}:${process.env.MONGO_PWD}@${process.env.MONGO_HOST}:${process.env.MONGO_PORT}/${process.env.MONGO_DB}?authSource=${process.env.MONGO_AUTH_SOURCE}&authMechanism=SCRAM-SHA-1&w=majority`;
let db = mongoose.createConnection(mongoUrl, options);

db.once('connected', () => {
  console.log('Connection contact-tracing db success!');
  return db;
});
db.on('disconnected', (err) => {
  console.log(`Connection db disconnected ${err}`);
});
db.on('error', err => {
  console.log(`Error in mongodb connection: ${err}`);
});
process.on('exit', code => {
  db.close();
  console.log(`About to exit with code: ${code}`);
});
process.on('SIGINT', function () {
  console.log('Caught interrupt signal');
  process.exit();
});
const CT = db.useDb('contact-tracing');
const received = new Schema(
  {
    name: {
      type: String,
      required: true
    },
    key: {
      type: String,
      required: true
    }
  }, {
  collection: 'received',
  timestamps: true,
  versionKey: false
}
);
const receivedModel = CT.model('received', received, 'received');

const sent = new Schema(
  {
    name: {
      type: String,
      required: true
    },
    key: {
      type: String,
      required: true
    }
  }, {
  collection: 'sent',
  timestamps: true,
  versionKey: false
}
);
const sentModel = CT.model('sent', sent, 'sent');

const infected = new Schema(
  {
    name: {
      type: String,
      required: true
    },
    fromDate: {
      type: Date,
      required: true
    },
    toDate: {
      type: Date,
      required: true
    }
  }, {
  collection: 'infected',
  timestamps: true,
  versionKey: false
}
);
const infectedModel = CT.model('infected', infected, 'infected');

// ---------------------------------------DATE CONFIG---------------------------------------

Date.prototype.addDays = function (days) {
  const date = new Date(this.valueOf());
  date.setDate(date.getDate() + days);
  return date;
};

Date.prototype.removeDays = function (days) {
  const date = new Date(this.valueOf());
  date.setDate(date.getDate() - days);
  return date;
};

// ---------------------------------------SERVIDOR---------------------------------------

const express = require('express')
const morgan = require('morgan')
const app = express()
const port = 3000

app.use(
  morgan('dev'),
  express.json()
)

app.post('/received', async (req, res) => {
  try {
    const { name, key } = req.body

    if (!key || typeof key !== 'string') {
      res.send('não criou')
    }

    console.log(`Name: ${name}, received key: ${key}`)

    const existent = await receivedModel.findOne({ name, key }).lean().exec()

    if (!existent) {
      receivedModel.create({ name, key })
      res.send('criou')
      return
    }

    res.send('não criou')
  } catch (error) {
    console.log('\x1b[31m%s\x1b[0m', 'Error in GET /received', error);
    res.status(400).send("Error in GET /received")
  }
})

app.post('/sent', async (req, res) => {
  try {
    const { name } = req.body
    const size = Number(process.env.KEY_SIZE) || 20

    const chars = '0123456789abcdefghijklmnopqrstuvwxyz'
    let key = ''
    for (let i = size; i > 0; --i) {
      key += chars[Math.floor(Math.random() * chars.length)]
    }

    sentModel.create({ name, key })

    console.log(`Name: ${name}, new key: ${key}`)

    res.send(key)
  } catch (error) {
    console.log('\x1b[31m%s\x1b[0m', 'Error in GET /sent', error);
    res.status(400).send("Error in GET /sent")
  }
})

app.post('/infected', async (req, res) => {
  try {
    const { name } = req.body
    console.log(`Infected name: ${name}`)

    const daysRange = Number(process.env.INF_DAYS_RANGE) || 7
    let today = new Date()

    await infectedModel.updateOne(
      {
        name
      },
      {
        $set: {
          fromDate: today.removeDays(daysRange),
          toDate: today.addDays(daysRange)
        }
      },
      { upsert: true }
    )

    res.send('infectado')
  } catch (error) {
    console.log('\x1b[31m%s\x1b[0m', 'Error in GET /infected', error);
    res.status(400).send("Error in GET /infected")
  }
})

app.post('/infected-alert', async (req, res) => {
  try {
    const { name } = req.body
    const today = new Date()

    const infectedModules = await infectedModel.aggregate([
      {
        $match: {
          fromDate: { $lt: today },
          toDate: { $gt: today }
        }
      },
      {
        $lookup: {
          from: 'sent',
          let: { from: '$fromDate', to: '$toDate', name: '$name' },
          pipeline: [
            {
              $match: {
                $expr: {
                  $and: [
                    { $eq: ['$name', '$$name'] },
                    { $lt: ['$createdAt', '$$to'] },
                    { $gt: ['$createdAt', '$$from'] }
                  ]
                }
              }
            },
            {
              $group: {
                _id: null,
                keys: { $push: '$key' }
              }
            }
          ],
          as: 'keys'
        }
      },
      {
        $set: {
          keys: '$keys.keys'
        }
      },
      { $unwind: '$keys' }
    ]).exec()

    if (infectedModules?.length) {
      const seconds = Number(process.env.KEYS_SEQ_INTERVAL) || 25
      const interval = 1000 * seconds;
      let alert = false;
      const consecutiveMin = Number(process.env.KEYS_SEQ_MIN) || 6

      for (let module of infectedModules) {
        if (!alert) {
          if (module?.keys.length) {
            let matchInfected = await receivedModel.find({ name, key: { $in: module.keys } }).lean().exec()

            if (matchInfected?.length) {
              let counter = 1;
              let lastTime = matchInfected[0].createdAt;

              matchInfected.every(key => {
                if ((key.createdAt - lastTime) < interval) {
                  counter++

                  if (counter >= consecutiveMin) {
                    return false
                  }
                } else {
                  counter = 1
                }
                
                lastTime = key.createdAt
                return true
              })

              if (counter >= consecutiveMin) {
                alert = true;
              }
            }
          }
          continue
        }
        break
      }

      if (alert) {
        console.log(`The ESP32 ${name} could be infected`)
        res.send('true')
        return
      }
    }

    console.log(`The ESP32 ${name} is safe`)
    res.send('false')
  } catch (error) {
    console.log('\x1b[31m%s\x1b[0m', 'Error in GET /infected-alert', error);
    res.status(400).send("Error in GET /infected-alert")
  }
})

app.listen(port, () => {
  console.log(`Express server listening on port ${port}`)
  const used = process.memoryUsage().heapUsed / 1024 / 1024;
  let txtUsed = `The script uses approximately ${used.toFixed(2)} MB`;
  console.log(`${txtUsed}`);
})