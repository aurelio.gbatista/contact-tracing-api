#include <WiFi.h>
#include <HTTPClient.h>
#include <BLEDevice.h>
#include <BLEServer.h>

// ESP32 BLE NAME
String espName = "ESP32-S01"; // CHANGE THE NAME BETWEEN DIFFERENT ESP32 MODULES

// WIFI PARAMETERS
const char *ssid = "Aurelio-EXT";
const char *password = "banana123";
const char *serverName = "http://192.168.1.177:3000";
String body = "{\"name\":\"" + espName + "\"}";

// BLE PARAMERTERS
BLEAdvertisementData advert;
BLEAdvertising *pAdvertising;
char key[21];

// LOOP TIME CONTROL PARAMETERS
unsigned long lastTimeLoop = millis();
unsigned long delayBetweenKeys = (1000 * 10);
bool firstLoop = true;

// FUNÇÃO POST HTTP - TROCA CHAVE AUTOMATICAMENTE
void httpPost(String route, String body) {
  if (WiFi.status() == WL_CONNECTED) {
    WiFiClient client;
    HTTPClient http;
    String url = serverName + route;
    http.begin(client, url);
    http.addHeader("Content-Type", "application/json");
    int httpResponseCode = http.POST(body);

    if (httpResponseCode > 0 && route == "/sent") {
      String payload = http.getString();
      payload.toCharArray(key, 21);
      http.end();
      return;
    }

    http.end();
    return;
  }
  else {
    Serial.println("WiFi Disconnected");
    return;
  }
}

// FUNÇÃO PARA ADICIONAR A CHAVE NO PACOTE ADVERTISING
void setAdvertisingKey(String c, BLEAdvertisementData &adv) {
  String s;
  s.concat(c);
  adv.setManufacturerData(s.c_str());
}

void setup() {
  Serial.begin(115200);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("WiFi on");

  BLEDevice::init("ESP32-CT-BLE");
  BLEServer *pServer = BLEDevice::createServer();
  pAdvertising = pServer->getAdvertising();
  advert.setName("ESP32-CT-BLE");
  pAdvertising->setAdvertisementData(advert);
  pAdvertising->start();
  Serial.println("BLE Server on, advertising...");
}

void loop() {
  if (Serial.available()) {
    String command = Serial.readStringUntil('\n');
    command.trim();
    if (command.equals("infected")) {
      httpPost("/infected", body);
      Serial.println("POST /infected (" + espName + ")");
    }
  }

  BLEAdvertisementData scan_response;
  setAdvertisingKey(key, scan_response);
  pAdvertising->stop();
  Serial.println("Stopped");
  pAdvertising->setScanResponseData(scan_response);

  unsigned long timeNow = millis();
  if ((timeNow - lastTimeLoop > delayBetweenKeys) || firstLoop == true) {
    httpPost("/sent", body);
    Serial.println("POST /sent - New key aquired");
    lastTimeLoop = timeNow;
    firstLoop = false;
  }
  else {
    delay(500);
  }

  pAdvertising->start();
  Serial.println("Start advertising with key: ");
  Serial.print(key);
  Serial.println("\n");
  delay(500);
}