#include <WiFi.h>
#include <HTTPClient.h>
#include <BLEDevice.h>
#include <BLEScan.h>
#include <BLEAdvertisedDevice.h>

// ESP32 BLE NAME
const String espName = "ESP32-SC00";

// WIFI PARAMETERS
const char *ssid = "Aurelio-EXT";
const char *password = "banana123";
const char *serverName = "http://192.168.1.177:3000";

// BLE PARAMERTERS
BLEScan *pBLEScan;
String lastKey;

// FUNÇÃO POST HTTP
void httpPost(String route, String body) {
  if (WiFi.status() == WL_CONNECTED) {
    WiFiClient client;
    HTTPClient http;
    String url = serverName + route;
    http.begin(client, url);
    http.addHeader("Content-Type", "application/json");
    int httpResponseCode = http.POST(body);

    if (httpResponseCode > 0 && route == "/infected-alert") {
      String payload = http.getString();
      if (payload == "true") {
        digitalWrite(2, HIGH);
      }
      http.end();
      return;
    }

    http.end();
    return;
  }
  else {
    Serial.println("WiFi Disconnected");
    return;
  }
}

// CALLBACK PARA APARELHOS ENCONTRADOS NO SCAN
class MyAdvertisedDeviceCallbacks : public BLEAdvertisedDeviceCallbacks {
  void onResult(BLEAdvertisedDevice advertisedDevice) {
    String deviceName = advertisedDevice.getName().c_str();
    Serial.println("Found new advertise");

    if (deviceName == "ESP32-CT-BLE") {
      if (advertisedDevice.haveManufacturerData() == true) {
        BLEAddress address = advertisedDevice.getAddress();
        uint8_t *payloadRaw = advertisedDevice.getPayload();
        size_t payloadLength = advertisedDevice.getPayloadLength();
        uint8_t bodyKey[20] = {};

        for (int i = 16; i < payloadLength; i++) {
          // if (payloadRaw[i] < 16) {
          //   Serial.print("0-");
          // }
          // Serial.print(payloadRaw[i], HEX);
          bodyKey[i - 16] = payloadRaw[i];
        }

        String finalKey = (char *)bodyKey;
        finalKey = finalKey.substring(0, 20);

        // Serial.println();
        // Serial.println(finalKey);

        if (lastKey != finalKey) {
          lastKey = finalKey;
          String body = "{\"name\":\"" + espName + "\",\"key\":\"" + finalKey + "\"}";
          httpPost("/received", body);
          Serial.println("POST /key - " + finalKey);
        }
        else {
          // Serial.println("Same key");
        }
      }

      // Serial.println("----------------------");
      return;
    }
  }
};

void setup() {
  pinMode(2, OUTPUT);
  Serial.begin(115200);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("WiFi on");

  BLEDevice::init("");
  pBLEScan = BLEDevice::getScan();
  pBLEScan->setAdvertisedDeviceCallbacks(new MyAdvertisedDeviceCallbacks());
  pBLEScan->setActiveScan(true);
  pBLEScan->setInterval(300);
  pBLEScan->setWindow(150);

  Serial.println("BLE Scan on, scanning...");
}

void loop() {
  pBLEScan->stop();
  Serial.println("Stopped scanning");
  pBLEScan->clearResults();
  Serial.println("POST /infected-alert");
  String body = "{\"name\":\"" + espName + "\"}";
  httpPost("/infected-alert", body);
  Serial.println("Scanning for 15 seconds");
  pBLEScan->start(15, false);
}